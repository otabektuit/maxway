import { combineReducers } from "redux";
import authReducer from "./authReducer";
import { persistReducer } from "redux-persist";
import commonReducer from './commonReducer'
import storage from "redux-persist/lib/storage";

const rootPersistConfig = {
    key: "root",
    storage,
    whitelist: ["user-info"],

};
const authPersistConfig = {
    key: "user-info",
    storage,
    whitelist: ["user"],
};

const commonPersistConfig = {
    key: "user-info",
    storage,
    whitelist: ["lesson"],
}


const rootReducer = combineReducers({
    common: commonReducer,
    auth: persistReducer(authPersistConfig, authReducer),
});

export default persistReducer(rootPersistConfig, rootReducer);
