import { authActionTypes } from "../actions/authActions/authActionTypes";

const initialState = {
    user: null,
    isModalOpen: false,
    token: "",
    phone: "",
    secret: "",
    profile: null
};

const authReducer = (state = initialState, action) => {
    const { payload } = action;
    switch (action.type) {
        case authActionTypes.SET_USER:
            return {
                ...state,
                user: payload,
            };
        case authActionTypes.LOGOUT:
            return {
                ...state,
                user: null,
            };
        case authActionTypes.PHONE:
            return {
                ...state,
                phone: payload
            }
        case authActionTypes.TOKEN:
            return {
                ...state,
                token: payload
            }
        case authActionTypes.SECRET:
            return {
                ...state,
                secret: payload
            }
        case authActionTypes.SET_PROFILE:
            return {
                ...state,
                profile: payload
            }
        default:
            return state;
    }
};

export default authReducer;
