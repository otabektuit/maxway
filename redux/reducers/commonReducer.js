import { commonActionTypes } from "../actions/commonActions/commonActionTypes";

const initialState = {
    loading: true,
    chapterIndex: 0,
    lessonIndex: 0,
    current_time: 0.1,
    progress: 0,
    student_lesson : null,
    next_lesson: ''
};

const commonReducer = (state = initialState, action) => {
    const { payload } = action;
    switch (action.type) {
        case commonActionTypes.LOADING:
            return {
                ...state,
                loading: payload
            }
        case commonActionTypes.CHAPTER_INDEX:
            return {
                ...state,
                chapterIndex: payload
            }
        case commonActionTypes.LESSON_INDEX:
            return {
                ...state,
                lessonIndex: payload
            }
        case commonActionTypes.CURRENT_TIME_PLAYER:
            return {
                ...state,
                current_time: payload
            }
        case commonActionTypes.SET_PROGRESS:
            return {
                ...state,
                progress: payload
            }
        case commonActionTypes.STUDENT_LESSON:
            return {
                ...state,
                student_lesson: payload
            }
        case commonActionTypes.NEXT_LESSON:
            return {
                ...state,
                next_lesson: payload
            }
        case commonActionTypes.CLEAR_COMMMON:
            
            return {
                ...state,
                loading: false,
                chapterIndex: 0,
                lessonIndex: 0,
                current_time: 0.1,
                progress: 0
            };
        default:
            return state;
    }
};

export default commonReducer;
