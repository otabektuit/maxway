import { authActionTypes } from "./authActionTypes";

export const setUser = (user) => {
    return {
        type: authActionTypes.SET_USER,
        payload: user,
    };
};

export const setToken = (token) => {
    return {
        type: authActionTypes.TOKEN,
        payload: token,
    };
};

export const setProfile = (profile) => {
    return {
        type: authActionTypes.SET_PROFILE,
        payload: profile,
    };
};


export const phoneAction = (phone) => {
    return {
        type: authActionTypes.PHONE,
        payload: phone,
    };
};


export const logout = () => ({
    type: authActionTypes.LOGOUT,
});


export const setSecret = (secret) => {
    return {
        type: authActionTypes.SECRET,
        payload: secret
    }
};





// export const openModal = () => ({
//     type: authActionTypes.OPEN_MODAL,
// });
// export const closeModal = () => ({
//     type: authActionTypes.CLOSE_MODAL,
// });
