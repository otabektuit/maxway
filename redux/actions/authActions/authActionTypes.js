export const authActionTypes = {
    SET_USER: "SET_USER",
    LOGOUT: "LOGOUT",
    PHONE: "PHONE",
    TOKEN: "TOKEN",
    SECRET: "SECRET",
    NUMBER: "NUMBER",
    SET_PROFILE: "SET_PROFILE",
    
    // OPEN_MODAL: "OPEN_MODAL",
    // CLOSE_MODAL: "CLOSE_MODAL",
};
