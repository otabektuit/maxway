import { commonActionTypes } from "./commonActionTypes";

export const isLoading = (loading) => {
    return {
        type: commonActionTypes.LOADING,
        payload: loading,
    };
};

export const setChapterIndex = (chapter_index) => {
    return {
        type: commonActionTypes.CHAPTER_INDEX,
        payload: chapter_index
    }
}

export const setLessonIndex = (lesson_index) => {
    return {
        type: commonActionTypes.LESSON_INDEX,
        payload: lesson_index
    }
}

export const setCurrentTimeStore = (time) => {
    return {
        type: commonActionTypes.CURRENT_TIME_PLAYER,
        payload: time
    }
}


export const setProgress = (progress) => {
    return {
        type: commonActionTypes.SET_PROGRESS,
        payload: progress
    }
};

export const setStudentLesson = (student_lesson) => {
    return {
        type: commonActionTypes.STUDENT_LESSON,
        payload: student_lesson
    }
};

export const commonClear = () =>{
    return {
        type: commonActionTypes.CLEAR_COMMMON
    };
}

export const setNextLesson = (next_lesson) =>{
    return {
        type: commonActionTypes.NEXT_LESSON,
        payload: next_lesson
    };
}




// export const openModal = () => ({
//     type: authActionTypes.OPEN_MODAL,
// });
// export const closeModal = () => ({
//     type: authActionTypes.CLOSE_MODAL,
// });
