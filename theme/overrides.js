// eslint-disable-next-line import/no-anonymous-default-export

const primary_color = '#51267D'
const secondary_color = '#FDC100'

export default {
  MuiToolbar: {
    regular: {
      '@media (min-width: 600px)': {
        minHeight: '56px',
      },
    },
  },
  MuiAppBar: {
    colorDefault: {
      backgroundColor: '#4B5B7A',
    },
  },
  MuiIconButton: {
    root: {
      padding: '18px',
      '&:hover': {
        backgroundColor: 'transparent',
      },
    },
    colorInherit: {
      color: '#8A99B6',
    },
    colorPrimary: {
      color: primary_color,
    },
  },
  MuiSvgIcon: {
    root: {
      margin: '0 auto',
      fontSize: '18px',
    },
    fontSizeInherit: {
      fontSize: '17px',
    },
  },
  MuiTypography: {
    root: {
      color: '#fff',
    },
  },
  MuiButton: {
    root: {
      fontSize: '14px',
      fontWeight: 500,
      lineHeight: '16px',
      borderRadius: '8px',
      textTransform: 'none',
      letterSpacing: 'normal',
    },
    contained: {
      boxShadow: 'none',
      color: '#FFF',
      backgroundColor: primary_color,
      '&:hover': {
        boxShadow: 'none',
        backgroundColor: '#51267dbf',
      },
    },
    text: {
      '&:hover': {
        backgroundColor: 'inherit',
      },
    },
    startIcon: {
      marginRight: 12,
    },
    outlined: {
      backgroundColor: '#51267d33',
      color: primary_color,
      borderColor: 'transparent',
      '&:hover': {
        backgroundColor: '#51267d22',
        color: primary_color,
      },
    },
  },
  MuiTextField: {
    root: {
      marginTop: 16,
    },
  },
  MuiOutlinedInput: {
    root: {
      borderRadius: 8,
      backgroundColor: '#F9F9F9',
      '&:hover .MuiOutlinedInput-notchedOutline': {
        borderColor: '#CFCFCF',
      },
      '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
        border: '1px solid #4993DD',
      },
    },
    notchedOutline: {
      borderColor: '#CFCFCF',
    },
  },
  MuiInputBase: {
    root: {
      color: '#6F6F6F',
      fontSize: '14px',
      fontWeight: 500,
      lineHeight: '16px',
      letterSpacing: 'normal',
    },
    input: {
      '&::placeholder': {
        color: '#BDBDBD',
      },
    },
  },
  MuiTreeView: {},
  MuiTreeItem: {
    root: {
      // padding: `0 10px`,
      '&:focus > .MuiTreeItem-content .MuiTreeItem-label': {
        backgroundColor: 'transparent',
      },
    },
    label: {
      width: `calc(100% - 42px)`,
      '&:hover': {
        backgroundColor: 'transparent',
      },
    },
    iconContainer: {
      width: 42,
      height: 42,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
  },
}
