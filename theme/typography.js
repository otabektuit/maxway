// eslint-disable-next-line import/no-anonymous-default-export
export default {
  body1: {
    fontFamily: '"PT-Sans-Caption", sans-serif',
    fontSize: '14px',
    fontWeight: 500,
    lineHeight: '16px',
  },

  h1: {
    fontFamily: '"PT-Sans-Caption", sans-serif',
    fontWeight: 500,
    fontSize: 35,
  },
  h2: {
    fontFamily: '"PT-Sans-Caption", sans-serif',
    fontWeight: 500,
    fontSize: 29,
  },
  h3: {
    fontFamily: '"PT-Sans-Caption", sans-serif',
    fontWeight: 500,
    fontSize: 24,
  },
  h4: {
    fontFamily: '"PT-Sans-Caption", sans-serif',
    fontWeight: 500,
    fontSize: 18,
    lineHeight: '21px',
  },
  h5: {
    fontFamily: '"PT-Sans-Caption", sans-serif',
    fontWeight: 500,
    fontSize: 14,
  },
  h6: {
    fontFamily: '"PT-Sans-Caption", sans-serif',
    fontWeight: 500,
    fontSize: 12,
    lineHeight: '16px',
  },

  overline: {
    fontWeight: 500,
  },
}
