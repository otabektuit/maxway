import { createMuiTheme, colors } from '@material-ui/core'
import typography from './typography'
import overrides from './overrides'

const theme = createMuiTheme({
  palette: {
    type: 'light',
    background: {
      default: '#EAEAEA',
      paper: colors.common.white,
    },
    primary: {
      main: '#51267D',
    },
    secondary: {
      main: '#FDC100',
    },
    text: {
      primary: '#6F6F6F',
      secondary: '#BDBDBD',
    },
  },
  options: {
    border: {
      radius: 5,
      rounded: '50%',
    },
  },
  typography,
  overrides,
  props: {
    MuiToolbar: {
      disableGutters: true,
    },
    MuiButton: {
      variant: 'text',
      disableRipple: true,
      disableTouchRipple: true,
      disableElevation: true,
    },
    MuiIconButton: {
      disableRipple: true,
      disableFocusRipple: true,
      disableTouchRipple: true,
      color: 'inherit',
    },
    MuiTypography: {
      variant: 'body1',
      color: 'textPrimary',
    },
  },
})

export default theme
