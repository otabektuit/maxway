import React from 'react'
import Footer from '../UI/Footer/Footer'
import Header from '../UI/Header/Header'

const MainLayout = (props) => {
  const { children } = props
  return (
    <>
      <Header />
      {children}
      <Footer />
    </>
  )
}

export default MainLayout
