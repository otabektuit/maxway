import { Box, Button, makeStyles, Typography } from '@material-ui/core'
import React from 'react'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    cursor: 'pointer',
    overflow: 'hidden',
    '&:hover img': {
      transform: 'scale(1.1)',
    },
  },
  header: {
    width: '100%',
    '& img': {
      width: '100%',
      objectFit: 'contain',
      transition: '0.4s ease all',
    },
  },
  footer: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  title: {
    marginBottom: 10,
  },
}))

const ProductCard = (props) => {
  const {
    data: {
      img = 'https://cdn.yaponamama.uz/products/thumbs/86_1615349947.jpg',
      title = 'Сет Minaku 38 шт.',
      about = 'Одним словом – классика. Содержит в себе роллы, которые точно Вас не разочаруют. Идеально для вечеринки в компании друзей.',
      price = '192 700',
    },
  } = props

  const classes = useStyles()
  return (
    <div className={classes.root}>
      <div className={classes.header}>
        <img src={img} alt={title} width={`100%`} height={`100%`} />
      </div>

      <Box mt={1} mb={2}>
        <Typography variant={'h3'} color={`primary`} className={classes.title}>
          {title}
        </Typography>
        <Typography variant={'h4'}>{about}</Typography>
      </Box>

      <Box className={classes.footer}>
        <Typography variant={`h3`} color={`primary`}>
          {price} сум
        </Typography>
        <Button variant={`contained`}>Выбрать</Button>
      </Box>
    </div>
  )
}

export default ProductCard
