import { Box, Container, Grid, makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import Link from 'next/link'
import FacebookIcon from '@material-ui/icons/Facebook'
import InstagramIcon from '@material-ui/icons/Instagram'
import TelegramIcon from '@material-ui/icons/Telegram'
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: '#f6f6f6',
    marginTop: '40px',
    position: 'relative',
  },
  list: {
    width: '100%',
    display: 'flex',
    listStyle: 'none',
    margin: 0,
    padding: 0,
  },
  item: {
    padding: `0 10px 10px 10px`,
    cursor: 'pointer',
    '&:hover': {
      color: theme.palette.primary.main,
    },
  },
  socials: {
    width: '100%',
    display: 'flex',
    listStyle: 'none',
    margin: 0,
    padding: '0',
  },
  social: {
    padding: '10px',
    cursor: 'pointer',
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: theme.options.border.radius,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: '0 15px 0 0',
    '& svg': {
      fontSize: 24,
      color: 'gray',
    },
    '&:hover svg': {
      color: theme.palette.primary.main,
    },
  },
  totop: {
    position: 'absolute',
    right: 20,
    top: 30,
    transform: 'rotate(-90deg)',
    fontSize: 32,
    transition: '.4s ease all',
    cursor: 'pointer',

    '&:hover': {
      transform: 'rotate(-90deg) translateX(5px)',
      color: theme.palette.primary.main,
    },
  },
  developed: {
    cursor: 'pointer',
    '&:hover': {
      color: theme.palette.primary.main,
    },
  },
  lastbox: {
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-end',
  },
}))

const Footer = () => {
  const classes = useStyles()

  return (
    <footer className={classes.root}>
      <Container maxWidth>
        <Grid container>
          <Grid item lg={4} md={4} sm={6} xs={6}>
            <Link href={`/`}>
              <img
                src={`/images/logo_c-1.png`}
                alt={'footer logo'}
                width={150}
                height={150}
              />
            </Link>

            <Box>
              <ul className={classes.list}>
                <li>
                  <Link href={`/`}>
                    <Typography variant={`h4`} className={classes.item}>
                      О нас
                    </Typography>
                  </Link>
                </li>
                <li>
                  <Link href={`/`}>
                    <Typography variant={`h4`} className={classes.item}>
                      Филиалы
                    </Typography>
                  </Link>
                </li>
                <li>
                  <Link href={`/`}>
                    <Typography variant={`h4`} className={classes.item}>
                      Контакты
                    </Typography>
                  </Link>
                </li>
              </ul>
            </Box>
          </Grid>
          <Grid item lg={4} md={4} sm={6} xs={6}>
            <Box mt={4}>
              <Typography variant={`h3`}>Мы в социальных сетьях</Typography>
            </Box>
            <Box mt={2}>
              <ul className={classes.socials}>
                <li className={classes.social}>
                  <Link href={`/`}>
                    <FacebookIcon />
                  </Link>
                </li>
                <li className={classes.social}>
                  <Link href={`/`}>
                    <InstagramIcon />
                  </Link>
                </li>
                <li className={classes.social}>
                  <Link href={`/`}>
                    <TelegramIcon />
                  </Link>
                </li>
              </ul>
            </Box>
          </Grid>
          <Grid item lg={4} md={4} sm={6} xs={6}>
            <Box mt={4}>
              <Typography variant={`h3`}>Заказывайте по номеру</Typography>
            </Box>
            <Box mt={2}>
              <Link href={`tel: +998998154828`}>
                <Typography variant={`h4`} style={{ cursor: 'pointer' }}>
                  +998 99 815 48 28
                </Typography>
              </Link>
            </Box>
          </Grid>
          <hr
            style={{
              width: '100%',
              height: 1,
            }}
          />
          <Grid item lg={6} md={6} sm={6} xs={6}>
            <Box my={1}>
              <Typography variant={`h5`}>2021 - MaxWay</Typography>
            </Box>
          </Grid>
          <Grid item lg={6} md={6} sm={6} xs={6}>
            <Box my={1} className={classes.lastbox}>
              <Link href={`https://udevs.io/`}>
                <Typography variant={`h5`} className={classes.developed}>
                  Developed by Udevs
                </Typography>
              </Link>
            </Box>
          </Grid>
        </Grid>
      </Container>

      <ArrowRightAltIcon
        className={classes.totop}
        onClick={() => {
          window.scrollTo(0, 0)
        }}
      />
    </footer>
  )
}

export default Footer
