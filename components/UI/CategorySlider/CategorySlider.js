import { Box, Container, Grid, makeStyles, Typography } from '@material-ui/core'
import React, { useState } from 'react'
import Link from 'next/link'

const useStyles = makeStyles((theme) => ({
  root: {
    width: `100%`,
    position: 'sticky',
    top: 60,
    boxShadow: '0 0 4px 0.3px rgba(0,0,0,0.2)',
    backgroundColor: 'white',
    zIndex: '9999',
    // backgroundColor: theme.palette.secondary.main,
  },
  list: {
    width: `100%`,
    display: 'flex',
    listStyle: 'none',
    padding: '0',
    margin: '10px 0',
    overflowX: 'scroll',
  },
  item: {
    padding: ' 5px ',
    margin: '0 10px 0 0',
    color: theme.palette.primary.main,
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'center',
    transition: '.4s ease all',
    borderBottom: `3px solid transparent`,
    '&:hover': {
      color: theme.palette.secondary.main,
      '& img': {
        transform: 'scale(1.1)',
      },
    },
    '& img': {
      transition: '.4s ease all',
    },
  },
  active: {
    borderBottom: `3px solid ${theme.palette.primary.main}`,
    // borderRadius: theme.options.border.radius,
    color: theme.palette.primary.main + '!important',
  },
  box: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}))

const CategorySlider = () => {
  const classes = useStyles()

  const [list] = useState([
    {
      src: 'https://cdn.yaponamama.uz/products/thumbs/80_1610951872.jpg',
      titel: 'Новинки',
    },
    {
      src: 'https://cdn.yaponamama.uz/products/thumbs/1_1596504276.jpg',
      titel: 'Сеты',
    },
    {
      src: 'https://cdn.yaponamama.uz/products/thumbs/1_1596494521.jpg',
      titel: 'Роллы',
    },
    {
      src: 'https://cdn.yaponamama.uz/products/thumbs/1_1596491599.jpg',
      titel: 'Суши',
    },
    {
      src: 'https://cdn.yaponamama.uz/products/thumbs/80_1601041911.jpg',
      titel: 'Сашими',
    },
    {
      src: 'https://cdn.yaponamama.uz/products/thumbs/1_1598940341.jpg',
      titel: 'Market',
    },
    {
      src: 'https://cdn.yaponamama.uz/products/thumbs/1_1596499234.jpg',
      titel: 'Супы',
    },
    {
      src: 'https://cdn.yaponamama.uz/products/thumbs/1_1596497130.jpg',
      titel: 'Десерты',
    },
    {
      src: 'https://cdn.yaponamama.uz/products/thumbs/1_1596497182.jpg',
      titel: 'Соусы',
    },
    {
      src: 'https://cdn.yaponamama.uz/products/thumbs/1_1596694628.jpg',
      titel: 'Напитки',
    },
  ])

  const [active, setActive] = useState(0)

  const handleSelect = (i) => (e) => {
    setActive(i)
  }

  return (
    <div className={classes.root}>
      <Container maxWidth>
        <Grid container>
          <Grid item lg={12} xs={12}>
            <ul className={classes.list}>
              {list.map((item, i) => (
                <li
                  key={i}
                  className={`${i === active ? classes.active : ''} ${
                    classes.item
                  }`}
                  onClick={handleSelect(i)}
                >
                  <Link href={`/`}>
                    <Box className={classes.box} maxWidth>
                      <img
                        src={item.src}
                        alt={item.titel}
                        width={60}
                        height={60}
                      />
                      <Typography variant={`h5`} color={'primary'}>
                        {item.titel}
                      </Typography>
                    </Box>
                  </Link>
                </li>
              ))}
            </ul>
          </Grid>
        </Grid>
      </Container>
    </div>
  )
}

export default CategorySlider
