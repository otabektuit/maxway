import { Box, Container, Grid, makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import Link from 'next/link'
import cls from './Header.module.scss'
import theme from '../../../theme'
import LocalMallOutlinedIcon from '@material-ui/icons/LocalMallOutlined'
import PersonOutlineOutlinedIcon from '@material-ui/icons/PersonOutlineOutlined'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: '60px',
    boxShadow: '0 0 2px 0.3px rgba(0,0,0,0.1)',
    display: 'flex',
    // flexDirection: 'column',
    alignItems: 'center',
    // backgroundColor: theme.palette.primary.main,
    backgroundColor: 'white',
    // color: 'white',
    position: 'sticky',
    top: '0',
    overflow: 'hidden',
    zIndex: '9999',
  },
  list: {
    listStyle: 'none',
    display: 'flex',
    margin: 0,
    height: `100%`,
    alignItems: 'center',
  },
  box: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  profile: {
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-end',
    // color: 'white',
  },
  item: {
    padding: '10px 15px',
    color: theme.palette.primary.main,
    margin: 0,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
    '&:hover': {
      color: theme.palette.secondary.main,
    },
  },
  container: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
  },
  brand: {
    objectFit: 'contain',
    cursor: 'pointer',
  },
  profile_item: {
    borderLeft: `1px solid ${theme.palette.primary.main}`,
    padding: '0 12px',
    display: 'flex',
    alignItems: 'center',
  },
  profile_list: {
    padding: '0 5px',
    listStyle: 'none',
    display: 'flex',
  },
  profile_item_list: {
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
    position: 'relative',
    padding: '0 10px',
  },
  cart_icon: {
    fontSize: '30px',
    color: theme.palette.primary.main,
    transition: '0.4s ease all',
    '&:hover': {
      color: theme.palette.secondary.main,
    },
  },
  quantity: {
    position: 'absolute',
    top: -3,
    right: 5,
    width: 15,
    height: 15,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white ',
    backgroundColor: theme.palette.primary.main,
    fontSize: 14,
    fontWeight: 'bolder',
    borderRadius: theme.options.border.radius,
  },
  typography: {
    color: theme.palette.primary.main,
  },
}))

const Header = () => {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <Container maxWidth className={classes.container}>
        <Grid container alignItems={`center`} className={classes.container}>
          <Grid item lg={2}>
            <Link href={`/`}>
              <img
                src={`/images/Rectangle/Both.png`}
                alt={`Max way logo`}
                width={70}
                height={60}
                className={classes.brand}
              />
            </Link>
          </Grid>
          <Grid item lg={7} justify={`center`} className={classes.container}>
            <Box className={classes.box} className={classes.container}>
              <ul className={classes.list}>
                <li className={`${cls.link} ${classes.item}`}>
                  <Link href={`/`}>Главная</Link>
                </li>
                <li className={`${cls.link} ${classes.item}`}>
                  <Link href={`/`}>О нас</Link>
                </li>
                <li className={`${cls.link} ${classes.item}`}>
                  <Link href={`/`}>Филиалы</Link>
                </li>
                <li className={`${cls.link} ${classes.item}`}>
                  <Link href={`/`}>Контакты</Link>
                </li>
              </ul>
            </Box>
          </Grid>
          <Grid item lg={3} className={classes.container}>
            <Box className={classes.profile}>
              <Box className={classes.profile_item}>
                <Link href={`tel: +998998154828`}>
                  <Box
                    style={{
                      cursor: 'pointer',
                    }}
                  >
                    <Typography variant={`h4`} className={classes.typography}>
                      +998 99 815 48 28
                    </Typography>
                    <Typography className={classes.typography}>
                      Ежедневно с 10:00 до 02:00
                    </Typography>
                  </Box>
                </Link>
              </Box>
              <Box className={classes.profile_item}>
                <ul className={classes.profile_list}>
                  <li className={classes.profile_item_list}>
                    <LocalMallOutlinedIcon className={classes.cart_icon} />
                    <span className={classes.quantity}>0</span>
                  </li>
                  <li className={classes.profile_item_list}>
                    <PersonOutlineOutlinedIcon className={classes.cart_icon} />
                  </li>
                </ul>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </div>
  )
}

export default Header
