import { Box, Container, Grid, makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import ProductCard from '../ProductCard/ProductCard'

const useStyles = makeStyles((theme) => ({
  category: {
    borderLeft: `5px solid ${theme.palette.primary.main}`,
    padding: '10px 15px',
    fontWeight: 'bolder',
  },
}))

const ProductsContainer = () => {
  const classes = useStyles()
  return (
    <div className={``}>
      <Container maxWidth>
        <Grid container spacing={3}>
          <Grid item lg={12} md={12} sm={12} xs={12}>
            <Box mt={7}>
              <Typography
                variant={`h2`}
                className={classes.category}
                color={'primary'}
              >
                Сеты
              </Typography>
            </Box>
          </Grid>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <ProductCard data={{}} />
          </Grid>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <ProductCard data={{}} />
          </Grid>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <ProductCard data={{}} />
          </Grid>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <ProductCard data={{}} />
          </Grid>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <ProductCard data={{}} />
          </Grid>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <ProductCard data={{}} />
          </Grid>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <ProductCard data={{}} />
          </Grid>
        </Grid>
      </Container>
    </div>
  )
}

export default ProductsContainer
