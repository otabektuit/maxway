import { makeStyles } from '@material-ui/core'
import { ImageSearch } from '@material-ui/icons'
import React, { useState } from 'react'
import Slider from 'react-slick'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  img: {
    height: '60vh',
    objectFit: 'cover',
  },
}))

const Banner = () => {
  const classes = useStyles()

  const settings = {
    dots: true,
    infinite: true,
    arrows: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
  }

  const [list] = useState([
    {
      src: 'https://cdn.yaponamama.uz/sliders/slider_1610939712.png',
      alt: 'Banner img',
    },
    {
      src: 'https://cdn.yaponamama.uz/sliders/slider_1596494572.jpg',
      alt: 'Banner img',
    },
    {
      src:
        'https://dodopizza-a.akamaihd.net/static/Img/Banners/g_1596699677_aa3a1cb9a55f4997a6b7266a671c9fe9.jpeg',
      alt: 'Banner img',
    },
  ])

  return (
    <div className={classes.root}>
      <Slider {...settings}>
        {list.map((item, i) => (
          <div key={i}>
            <img
              src={item.src}
              width={`100%`}
              alt={item.alt}
              className={classes.img}
            />
          </div>
        ))}
      </Slider>
    </div>
  )
}

export default Banner
