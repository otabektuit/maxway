import React from 'react'
import Banner from '../UI/Banner/Banner'
import CategorySlider from '../UI/CategorySlider/CategorySlider'
import ProductsContainer from '../UI/ProductsContainer/ProductsContainer'

const HomePage = (props) => {
  return (
    <>
      <Banner />
      <CategorySlider />
      <ProductsContainer />
      <ProductsContainer />
      <ProductsContainer />
    </>
  )
}

export default HomePage
